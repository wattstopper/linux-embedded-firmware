# Docker build for gcc-arm-none-eabi usage in Ubuntu.
FROM ubuntu:bionic

ENV DEBIAN_FRONTEND noninteractive

RUN dpkg --add-architecture i386
RUN apt-get update && \
		apt-get -y install build-essential vim-common wget git bzip2 make python libc6:i386 \
		astyle clang clang-format clang-tools cppcheck splint \
		cmake python3 curl tzdata zip unzip \
		&& \
		apt-get -y clean && \
		apt-get -y autoclean
		
# arm compiler locations
# https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
# https://developer.arm.com/-/media/Files/downloads/gnu-rm/5_4-2016q2/gccarmnoneeabi542016q220160622linuxtar.bz2?revision=8f445a99-c1ae-4ed8-9eb8-f41929a671c4?product=GNU%20Arm%20Embedded%20Toolchain,32-bit,,Linux,5-2016-q2-update
# https://developer.arm.com/-/media/Files/downloads/gnu-rm/5_4-2016q3/gcc-arm-none-eabi-5_4-2016q3-20160926-linux.tar.bz2?revision=111dee36-f88b-4672-8ac6-48cf41b4d375?product=GNU%20Arm%20Embedded%20Toolchain,32-bit,,Linux,5-2016-q3-update
# https://developer.arm.com/-/media/Files/downloads/gnu-rm/6-2016q4/gcc-arm-none-eabi-6_2-2016q4-20161216-linux.tar.bz2?revision=241a583b-4a1b-4d69-845f-f1efb022974c?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,6-2016-q4-major
# https://developer.arm.com/-/media/Files/downloads/gnu-rm/6_1-2017q1/gcc-arm-none-eabi-6-2017-q1-update-linux.tar.bz2?revision=6799a2bc-ea25-4e11-8a36-3f4bce1c06f7?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,6-2017-q1-update
# https://developer.arm.com/-/media/Files/downloads/gnu-rm/6-2017q2/gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2?revision=2cc92fb5-3e0e-402d-9197-bdfc8224d8a5?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,6-2017-q2-update
# https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2?revision=108bd959-44bd-4619-9c19-26187abf5225&la=en&hash=E788CE92E5DFD64B2A8C246BBA91A249CB8E2D2D
# https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2?revision=05382cca-1721-44e1-ae19-1e7c3dc96118&la=en&hash=D7C9D18FCA2DD9F894FD9F3C3DC9228498FA281A,Linux x86_64 Tarball
		
RUN wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/6-2017q2/gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2 && \
 tar -xjf gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2 -C /usr/local/bin/ && rm *bz2 \
 && \
 wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2 && \
 tar -xjf gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2 -C /usr/local/bin/ && rm *bz2 \
 && \
 wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2 && \
 tar -xjf gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2 -C /usr/local/bin/ && rm *bz2

#RUN mkdir -p /usr/local/bin/

RUN ln -fs /usr/share/zoneinfo/America/Denver /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

# include the following two lines to have the docker image run indefinitely
# COPY ./loop.sh /
# ENTRYPOINT [ "/loop.sh" ] 

LABEL Name=linux-embedded-firmware Version=0.0.4
