
# Linux Embedded Firmware

This image contains the necessary cross compilers to build embedded firmware

## Contents

- [Linux Embedded Firmware](#linux-embedded-firmware)
  - [Contents](#contents)
  - [Features](#features)
  - [Usage](#usage)
  - [Examples](#examples)

## Features
* [ARM GNU Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) - multiple version of gcc-arm-non-eabi. 5.4 6.2. etc
* Extra tools - curl, tzdata, zip, unzip
* Timezone set to Moutnain Time

## Usage

[legrandbcs/linux-embedded-firmware](https://cloud.docker.com/u/legrandbcs/repository/docker/legrandbcs/linux-embedded-firmware)

## Examples